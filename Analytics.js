export const track = (event, data = {}) => {
	//
	console.info('Analytics.event called');
};

export const event = (target, name, descriptor) => {
	const sf = descriptor.value;

	descriptor.value = function(ev, ...args) {
		try {
			track();
		} catch (e) {
			console.warn(e);
		}
		return sf.bind(this)(ev, ...args);
	};

	return descriptor;
};

export const data = (event, data = {}) => {
	//
};
